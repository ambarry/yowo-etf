import com.yowo.ETF;
import com.yowo.Hap;
import java.io.IOException;
import java.nio.*;
import java.nio.channels.*;

public class EtfPrinter {
  public static void main(String[] args) throws IOException {

    WritableByteChannel channel = Channels.newChannel(System.out);

    ByteBuffer b = ETF.toETFSmallInt(5);
    System.out.println("Result from int 5:\n");
    printBytes(channel, b);

    ByteBuffer bInt = ETF.toETFInt(500);
    System.out.println("Result from int 500:\n");
    printBytes(channel, bInt);

    ByteBuffer bCharList = ETF.toETFCharList("Hello, world!");
    System.out.println("Result from 'Hello, world!' as char list:\n");
    printBytes(channel, bCharList);

    ByteBuffer bBinary = ETF.toETFBinary("Hello, world!");
    System.out.println("Result from 'Hello, world!' as binary:\n");
    printBytes(channel, bBinary);

    ByteBuffer bAtom = ETF.toETFAtom("this_atom");
    System.out.println("Result from 'this_atom' as atom:\n");
    printBytes(channel, bAtom);

    Hap hap = new Hap(1, "Free Apples on 2nd St.", "This is a hap about apples.", "70_50", "");
    ByteBuffer hapBuf = ETF.toETFHap(hap);
    System.out.println("Result from hap:\n");
    printBytes(channel, hapBuf);
  }

  public static void printBytes(WritableByteChannel channel, ByteBuffer buffer) throws IOException {
    channel.write(buffer);
    System.out.println();
  }
}
