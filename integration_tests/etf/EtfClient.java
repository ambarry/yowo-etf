/**
* Use separate threads so we can read and write to same socket
* at same time.
* Will read from StdIn and write this to socket.
* Will also read from socket and write to StdOut.
*/

import java.io.*;
import java.net.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.Map;
import com.yowo.*;


public class EtfClient {
  public static void main(String[] args) {
    if (args.length != 2) {
      System.err.println("Usage: java EtfClient <host name> <port number>");
      System.exit(1);
    }

    String hostName = args[0];
    int portNumber = Integer.parseInt(args[1]);

    new EtfClient().start(hostName, portNumber);
  }

  public void start(String hostName, int portNumber) {
    try (
      Socket echoSocket = new Socket(hostName, portNumber);
      //BufferedReader socketIn = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));
      DataInputStream iStream = new DataInputStream(echoSocket.getInputStream());
      OutputStream oStream = echoSocket.getOutputStream())
    {
      // launch new write thread
      SocketWriter swRunnable = new SocketWriter(oStream);
      Thread writeThread = new Thread(swRunnable);
      writeThread.start();
      // loop and read
      loop_read(iStream);
    } catch (UnknownHostException e) {
        System.err.println("Don't know about host " + hostName);
        System.exit(1);
    } catch (IOException e) {
        System.err.println("Couldn't get I/O for the connection to " + hostName);
        System.exit(1);
    }
  }

    public static void loop_read(DataInputStream iStream) throws IOException
    {
      // read loop (note: do at least one here to keep socket open!)
      String message = "";
      Map <String, Object> map;
      int len;
      while(true) {
        // use data input stream
        len = iStream.readInt();
        System.out.println("Length: " + len);
        byte[] bytes = new byte[len];
        iStream.read(bytes, 0, len);
        ByteBuffer buf = ByteBuffer.wrap(bytes);
        // try to decode ETF binary for now, eventuall do switch processing
        buf.get(); // 131
        byte b = buf.get();
        switch(b) {
          case ETF.ETF_BINARY:
            message = ETF.decodeETFBinary(buf);
            System.out.println("Received: " + message);
            break;
          case ETF.ETF_ATOM:
            message = ETF.decodeETFAtom(buf);
            System.out.println("Received: " + message);
            break;
          case ETF.ETF_MAP:
            map = ETF.decodeETFMap(buf);
            // TODO: check struct...
            for (String k : map.keySet()) {
              System.out.println("Key: " + k+ "\n");
            }
            break;
        }
    }
  }

  public class SocketWriter implements Runnable {
    private OutputStream oStream;
    public SocketWriter(OutputStream outputStream) {
      this.oStream = outputStream;
    }

    public void run() {
      try (
        PrintWriter socketOut = new PrintWriter(oStream, true);
        WritableByteChannel channel = Channels.newChannel(oStream);
        BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in))
      ) {
        // simple etf test
        ByteBuffer buf = ETF.toETFAtom("Hello, world!");
        WriteToElixir(channel, buf);

        ByteBuffer s = ETF.toETFAtom("__struct__");
        WriteToElixir(channel, s);

        // hap test
        Hap hap = new Hap(1, "Free Apples on 2nd St.", "This is a hap about apples.", "70_50", "");
        ByteBuffer hapBuf = ETF.toETFHap(hap);
        WriteToElixir(channel, hapBuf);

        // packet test
        Hap hap2 = new Hap(2000, "This is a title","This is a message", "60_60", "");
        ByteBuffer hapBuf2 = ETF.toETFHap(hap2);
        ByteBuffer packet = ETF.toETFPacket(Command.PUBLISH_HAP, hapBuf2);
        WriteToElixir(channel, packet);

        // echo string / binary loop
        String userInput;
        while ((userInput = stdIn.readLine()) != null) {
          System.out.println("sending: " + userInput);
          buf = ETF.toETFBinary(userInput);
          WriteToElixir(channel, buf);
        }
      }
      catch (IOException e) {
          System.err.println("Couldn't get I/O.");
          System.exit(1);
      }
    }

    public void WriteToElixir(WritableByteChannel channel, ByteBuffer msg) throws IOException {
      ByteBuffer header = ByteBuffer
        .allocate(5)
        .putInt(msg.limit() + 1)
        .put((byte)131);
      header.flip();
      channel.write(header);
      channel.write(msg);
    }
  }


}
