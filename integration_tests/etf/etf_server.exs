defmodule EtfServer do
  @moduledoc """
    A simple server to establish bi-directional communication
    with Java client.
    Since we're not just echoing, need to make socket active / active once
    so we don't block on recv, as with Yowo's server.
  """

  def accept port do
    opts = [:binary, packet: 4, active: :once, reuseaddr: true]
    {:ok, l_socket} = :gen_tcp.listen(port, opts)
    IO.puts "Accepting connections on port #{port}!!!"
    # starting a pseudo registry
    {:ok, reg_pid} = Task.start_link(fn -> loop_registry([]) end)
    Task.start_link(fn -> loop_acceptor(l_socket, reg_pid) end)
    reg_pid
  end

  @doc """
    Writes to all subs;
    first, sends message to registry, which
    loops subs and sends msg to each.
    The subs should write to socket.
  """
  def write(pid, msg) do
    send pid, {:write, msg}
  end

  defp loop_registry subs do
    receive do
      {:new_sub, sub} -> loop_registry ([sub | subs])
      {:remove_sub, sub} ->
        subs
        |> Enum.filter(fn x -> x != sub end)
        |> loop_registry()
      {:write, msg} ->
        subs
        |> Enum.each(fn s -> send(s, {:write, msg}) end)
        loop_registry subs
    end
  end

  defp loop_acceptor socket, registry do
    {:ok, client} = :gen_tcp.accept(socket)
    {:ok, pid} = Task.start_link(fn ->
      IO.puts "Received new client."
      serve(client, registry)
    end) # note: should be supervised in reality
    :ok = :gen_tcp.controlling_process(client, pid) # transfer control
    send registry, {:new_sub, pid} # add to registry
    loop_acceptor socket, registry
  end

  defp serve(socket, registry) do
    receive do
      {:write, msg} ->
        IO.puts "Sending message..."
        msg
        |> :erlang.term_to_binary()
        |> send_socket(socket)
      {:tcp, _s, contents} ->
        IO.puts "Received contents:\n"
        IO.puts(inspect contents)
        try do
          IO.puts "Attempting term conversion..."
          x = :erlang.binary_to_term(contents) # this will break on text but works on ETF test!
          IO.puts (inspect x)
        rescue
          ArgumentError -> IO.puts "Not an Erlang term!!!\n";
          IO.puts contents
        end
        :inet.setopts(socket, [active: :once]) # listen again
      {:tcp_closed, _s} ->
        IO.puts "Socket closed.\n"
        send registry, {:remove_sub, self()}
    end
    serve(socket, registry)
  end

  defp send_socket(msg, socket) do
    :gen_tcp.send(socket, msg)
  end
end
