defmodule EchoServer do
  @moduledoc """
    This is a simple echo server for testing the Java client.
  """

  def accept(port) do
    opts = [:binary, packet: :line, active: :false, reuseaddr: true]
    {:ok, l_socket} = :gen_tcp.listen(port, opts)
    IO.puts "Accepting connections on port #{port}!!!"
    loop_acceptor l_socket
  end

  # recursive accept clients loop
  defp loop_acceptor(l_socket) do
    {:ok, client} = :gen_tcp.accept(l_socket)
    Task.start_link(fn ->
      IO.puts "Received new client..."
      serve(client)
    end)
    loop_acceptor(l_socket)
  end

  # recursive echo loop
  defp serve(socket) do
    socket
    |> read_line()
    |> write_line(socket)

    serve(socket)
  end

  defp read_line(socket) do
    {:ok, data} = :gen_tcp.recv(socket, 0)
    IO.puts "Received data: #{data}"
    data
  end

  defp write_line(data, socket) do
    :gen_tcp.send(socket, data)
  end
end
