package com.yowo;

public enum Command {
  AUTHORIZE,
  PUBLISH_HAP,
  SUBSCRIBE,
  UNSUBSCRIBE
}
