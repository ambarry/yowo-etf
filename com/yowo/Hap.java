package com.yowo;

import java.util.Map;

public class Hap {
  public int user_id;
  public String title;
  public String message;
  public String topic;
  public String hap_id;

  public Hap() {
    user_id = 0;
    hap_id = "";
  }

  public Hap(int user_id, String title, String message, String topic, String hap_id) {
    this.user_id = user_id;
    this.title = title;
    this.message = message;
    this.topic = topic;
    this.hap_id = hap_id;
  }

  public Hap(Map<String, Object> map) {
    this.user_id = (int)(Integer)map.get("user_id");
    this.title = (String)map.get("title");
    this.message = (String)map.get("message");
    this.topic = (String)map.get("topic");
    this.hap_id = (String)map.get("hap_id");
    // this.lat = (double)map.get("lat");
    // this.lon = (double)map.get("lon");
  }
}
