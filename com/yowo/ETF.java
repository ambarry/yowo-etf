package com.yowo;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.HashMap;

public class ETF {

  public static final byte ETF_SMALL_INT = (byte)97;
  public static final byte ETF_INT = (byte)98;
  public static final byte ETF_ATOM = (byte)100;
  public static final byte ETF_CHAR_LIST = (byte)107;
  public static final byte ETF_BINARY = (byte)109;
  public static final byte ETF_MAP = (byte)116;

  private static short intToShort(int i) {
    short s = i > Short.MAX_VALUE ? Short.MAX_VALUE : (short)i;
    return s;
  }

  public static ByteBuffer toETFSmallInt(int i) {
    //97, the int as 1 byte
    ByteBuffer buf = ByteBuffer
      .allocate(2)
      .put(ETF_SMALL_INT)
      .put((byte)i);
    buf.flip();
    return buf;
  }

  public static int decodeETFSmallInt(ByteBuffer buf) {
    // we already read the 97
    // TODO: make sure this cast works, esp with the sign...
    int i = buf.get();
    return i;
  }

  public static ByteBuffer toETFInt(int i) {
    //98, the int as 4 bytes
    ByteBuffer buf = ByteBuffer
      .allocate(5)
      .put(ETF_INT)
      .putInt(i);
    buf.flip();
    return buf;
  }

  public static int decodeETFInt(ByteBuffer buf) {
    // already read the 98
    return buf.getInt();
  }

  public static ByteBuffer toETFCharList(String str){
    // NOTE: the ETF string really comes through as char list,
    // so this has been named accordingly. Strings should use ETF.toBinary

    // TODO: need to guarantee str < 255!!!
    //107, 2 bytes length, then all chars
    short len = intToShort(str.length());

    ByteBuffer buf = ByteBuffer
      .allocate(3 + len)
      .put(ETF_CHAR_LIST)
      .putShort(len)
      .put(str.getBytes(StandardCharsets.UTF_8));
    buf.flip();
    return buf;
  }

  public static String decodeETFCharList(ByteBuffer buf) {
    // already read the 107
    short len = buf.getShort();
    byte[] bytes = new byte[len];
    buf.get(bytes);
    return new String(bytes, StandardCharsets.UTF_8);
  }

  public static ByteBuffer toETFBinary(String str) {
    int len = str.length();
    ByteBuffer buf = ByteBuffer
      .allocate(5 + len)
      .put(ETF_BINARY)
      .putInt(len)
      .put(str.getBytes(StandardCharsets.UTF_8));
    buf.flip();
    return buf;
  }

  public static String decodeETFBinary(ByteBuffer buf) {
    // at this point, we've already read the 109 and know it's a binary
    // need to read length, then parse into string and return.
    // this will keep moving the buffer's current position, considering
    // it was created by wrapping the full array.
    int len = buf.getInt();
    byte[] bytes = new byte[len];
    buf.get(bytes);
    return new String(bytes, StandardCharsets.UTF_8);
  }

  public static ByteBuffer toETFAtom(String str) {
    // TODO: need to guarantee str < 255!!!
    // 100, 2 bytes len (max 255), atom name chars
    short len = intToShort(str.length());
    ByteBuffer buf = ByteBuffer
      .allocate(3 + len)
      .put(ETF_ATOM)
      .putShort(len)
      .put(str.getBytes(StandardCharsets.UTF_8));
    buf.flip();
    return buf;
  }

  public static String decodeETFAtom(ByteBuffer buf) {
    // already read the 100
    short len = buf.getShort();
    byte[] bytes = new byte[len];
    buf.get(bytes);
    return new String(bytes, StandardCharsets.UTF_8);
  }

  public static ByteBuffer toETFMap(int numPairs, ByteBuffer etfPairs) {
    // 116, arity in 4 bytes, the pairs as k1, v1, k2, v2, ... kn, vn
    // no duplicate keys allowed!!!
    ByteBuffer buf = ByteBuffer
      .allocate(5 + etfPairs.capacity())
      .put(ETF_MAP)
      .putInt(numPairs)
      .put(etfPairs);
    buf.flip();
    return buf;
  }

  public static Map<String, Object> decodeETFMap(ByteBuffer buf) {
    // we already have the 131 and 116
    int numPairs = buf.getInt();
    Map<String, Object> map = new HashMap<String, Object>(numPairs);
    String key;
    Object val;
    byte tag;

    for (int i = 0; i < numPairs; i++) {
      tag = buf.get(); // TODO: this SHOULD be an atom, but may need to check tag??
      key = decodeETFAtom(buf);
      val = decodeETFObject(buf);
      map.put(key, val);
    }
    return map;
  }

  public static ByteBuffer toETFHap(Hap hap) {
    // TODO: guarantee we won't overflow the buffer - keys are aourn 58 bytes at the moment, then vals
    ByteBuffer buffer = ByteBuffer.allocate(2000);
    String[] keys = new String[] {"__struct__", "user_id", "title", "message", "topic", "hap_id"};

    for(String k : keys) {
      buffer.put(toETFAtom(k));
      switch(k) {
        case "__struct__": buffer.put(toETFAtom("Elixir.Yowo.Hap"));
          break;
        case "user_id":
          if (hap.user_id < 256) {
            buffer.put(toETFSmallInt(hap.user_id));
          } else {
            buffer.put(toETFInt(hap.user_id));
          }
          break;
        case "title": buffer.put(toETFBinary(hap.title));
          break;
        case "message": buffer.put(toETFBinary(hap.message));
          break;
        case "topic": buffer.put(toETFBinary(hap.topic));
          break;
        case "hap_id": buffer.put(toETFBinary(hap.hap_id));
          break;
        default: break;
      }
    }
    buffer.flip();
    return toETFMap(keys.length, buffer);
  }

  public static ByteBuffer toETFPacket(Command command, ByteBuffer data) {
    ByteBuffer buffer = ByteBuffer
      .allocate(2000)
      .put(toETFAtom("__struct__"))
      .put(toETFAtom("Elixir.Yowo.Packet"))
      .put(toETFAtom("command"))
      .put(toETFBinary(command.toString()))
      .put(toETFAtom("data"))
      .put(data);
    buffer.flip();
    return toETFMap(3, buffer);
  }

  public static Object decodeETFObject(ByteBuffer buf) {
    Object res;
    byte tag = buf.get();

    switch(tag) {
      case ETF_ATOM:
        res = decodeETFAtom(buf);
        break;
      case ETF_MAP:
        res = decodeETFMap(buf);
        break;
      case ETF_BINARY:
        res = decodeETFBinary(buf);
        break;
      case ETF_CHAR_LIST:
        res = decodeETFCharList(buf);
        break;
      case ETF_SMALL_INT:
        res = decodeETFSmallInt(buf);
        break;
      case ETF_INT:
        res = decodeETFInt(buf);
        break;
      default:
        res = null;
        break;
    }
    return res;
  }
}

//
// subscribe and unsubscribe:
// %{:topic => topic}
//
//
// authorize
// user_id integer
